package edu.nus.cs5248;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.MediaController;
import android.widget.MediaController.MediaPlayerControl;
import android.widget.TextView;
import android.widget.Toast;

public class PlayerActivity extends Activity implements OnCompletionListener,
SurfaceHolder.Callback, MediaPlayerControl  {
	private static final String TAG = "PlayerActivity";

	String mpdUrl = null;
	private boolean isMpdParsed;
	
	// View layout elements
	ProgressDialog progressDialog;
	TextView tvSegmentInfo;
	TextView tvQualityInfo;
	TextView tvSpeedInfo;
	
	// store statistics for each segment
	float[] rawBandwidth;
	float[] smoothenBandwidth;
	float[] currQuality;
	int[] buff_usage;
	float[] inputBandwidth;
	float[] realBandwidth;
	private float recordedRawBw = 0;
	private float recordedSmoothBw = 0;
	private int curr_buff_usage;
	
	// record total time spent to play the video (includes time spent in buffering)
	long startPlayTimeMs;
	long endPlayTimeMs;
	
	// buffer threshold values
	private static int BUFFER_MIN = 5;
	private static int MED_TH = 8;	
	private static int BUFFER_MAX = 11;
	
	// Contains the urls to the streamlets
	List<String> hyperlinks = new ArrayList<String>();
	List<String> urls_lo = new ArrayList<String>();
	List<String> urls_med = new ArrayList<String>();
	List<String> urls_hi = new ArrayList<String>();
	
	// Contains the bandwidth stated in MPD playlist
	int rate_lo = 0;
	int rate_med = 0;
	int rate_hi = 0;
	
	// Index of the streamlet being downloaded
	int seq = 0;
	// Index of the streamlet being played
	private int currentVideo = 0;
	
	// Path of the downloaded Videos
	ArrayList<String> videoPath = new ArrayList<String>();   
	// Save the quality of downloaded segment
	ArrayList<String> segmentQuality = new ArrayList<String>();   
	
	// Store previous bandwidth values for bandwidth estimation
	float bw;
	float prev_bw;
	float prev_prev_bw;
	
	// weights for bandwidth estimation
	private static final double ALPHA = 0.0;
	private static final double BETA = 0.9;

	// MediaPlayer stuff
	private MediaPlayer mediaPlayer;    
	private SurfaceHolder holder;
    private Handler handler = new Handler();
    private MediaController mController;
    
    // check if first segment is played
	private Boolean startPlay = true;
	
	private int totalSegment = 0;

	// check if the player is waiting for buffer to be filled
	private boolean isBuffering;

	private boolean IS_USING_BANDWIDTH_ESTIMATION = true;
	private boolean IS_BANDWIDTH_SMOOTHENING_ENABLED = true;
	private boolean IS_BUFFER_THRESHOLDING_ENABLED = false;
	private boolean IS_SIMULATED_INPUT_ENABLED;
	
	private String inputBwFilePath = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		curr_buff_usage = 0;
		bw = 0;
		prev_bw = 0;
		prev_prev_bw = 0;
		
		Intent intent = getIntent();
		mpdUrl = intent.getStringExtra("url");
		inputBwFilePath = intent.getStringExtra("simulate_file");
		if (!inputBwFilePath.isEmpty()) {
			IS_SIMULATED_INPUT_ENABLED = true;
		} else {
			IS_SIMULATED_INPUT_ENABLED = false;
		}
		
		isMpdParsed = false;
		isBuffering = false;
		
		startPlayTimeMs = 0;
		endPlayTimeMs = 0;

		Toast.makeText(this, "urlPath: " + mpdUrl, Toast.LENGTH_LONG).show();
		
		setContentView(R.layout.activity_player);

		SurfaceView surface = (SurfaceView) findViewById(R.id.surfaceView);
		holder = surface.getHolder();
		holder.addCallback(this);
		holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
		
		tvSegmentInfo = (TextView) findViewById(R.id.TVsegmentInfo); 
		tvQualityInfo = (TextView) findViewById(R.id.TVqualityInfo);
		tvSpeedInfo = (TextView) findViewById(R.id.TVspeedInfo);
		
		mController = new MediaController(this, false);
		mController.setPrevNextListeners(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// Next button pressed
				mController.show();
				
				if (currentVideo == seq-1) {
					return;
				}
				
				if (seq < (urls_hi.size()) && curr_buff_usage == BUFFER_MAX -1) {
					downloadNextSegmentAdaptively();
				}
				
				if (currentVideo < totalSegment-1) {
					currentVideo++;
					curr_buff_usage--;	
				}
				
				updateSegmentInfo();
				
				playVideo(videoPath.get(currentVideo));
			}
		}, new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				mController.show();
				// Prev button pressed
				if (currentVideo > 0) {
					currentVideo--;
				}

				updateSegmentInfo();
				
				playVideo(videoPath.get(currentVideo));
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		//		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}
	
	public void updateSegmentInfo() {
		tvSegmentInfo.setText("Played/Downloaded/Total streamlets: " + 
				currentVideo + "/" + seq + "/" + totalSegment +
				"\nBuffer usage: " + buff_usage[currentVideo]);
	}
	
	public void updateVidQualityInfo() {
		if (currQuality[currentVideo] == 0) {
			tvQualityInfo.setText("LOW quality (" + String.format("%.02f", ((float)rate_lo/(1000*1000))) + " MBps)");
		} else if (currQuality[currentVideo] == 1) {
			tvQualityInfo.setText("MED quality (" + String.format("%.02f", ((float)rate_med/(1000*1000))) + " MBps)");
		} else if (currQuality[currentVideo] == 2) {
			tvQualityInfo.setText("HI quality (" + String.format("%.02f", ((float)rate_hi/(1000*1000))) + " MBps)");
		}
	}
	
	public void updateSmoothenedBwInfo() {
		tvSpeedInfo.setText("Raw B/W: " + String.format("%.02f", rawBandwidth[currentVideo]/(1000*1000)) + " Mbps\n"
				+ "Smoothened B/W: " + String.format("%.02f", smoothenBandwidth[currentVideo]/(1000*1000)) + " Mbps");
	}
	
	private void showProgressDialog() {
		progressDialog = ProgressDialog.show(this, null,
			    "Buffering...", true);
	}
	
	private void hideProgressDialog() {
		progressDialog.dismiss();
	}

	// Class to download the segment and calculate download speed, 
	// first segment playback is initiated here as well
	private class DownloadSegmentTask extends AsyncTask<String, Void, Float> {

		protected Float doInBackground(String... urls) {
			try {				
				String fullUrl = mpdUrl.substring(0, mpdUrl.lastIndexOf('/')+1) + urls[0];
				URL url = new URL(fullUrl);
				HttpURLConnection urlConnection = (HttpURLConnection) url
						.openConnection();
				urlConnection.setRequestMethod("GET");
				urlConnection.setDoOutput(true);
				
				long startTime = System.currentTimeMillis();
				
				urlConnection.connect();
				InputStream inputStream = urlConnection.getInputStream();

				long endTime = System.currentTimeMillis();
				float dltime = (float) (((float) (endTime - startTime)) / 1000.0);

				// Store the downloaded segments in the cache directory
				File file = new File(getCacheDir(), seq + ".mp4");
				String value = file.getPath();
				Log.i(TAG, " file.getPath(): " + file.getPath());
				videoPath.add(value);

				FileOutputStream fileOutput = new FileOutputStream(file);
				byte[] buffer = new byte[1024];
				int bufferLength = 0;

				while ((bufferLength = inputStream.read(buffer)) > 0) {
					fileOutput.write(buffer, 0, bufferLength);
				}
				fileOutput.close();
				long contentLength = file.length();
				float spd1 = contentLength / dltime * 8;
				Integer spd = Math.round(spd1);   // Download speed of segments
				float dnldSpeed = ((float) spd );/// (1024*1024);
		
				realBandwidth[seq] = dnldSpeed; 
						
				if (IS_SIMULATED_INPUT_ENABLED) { 
					dnldSpeed = inputBandwidth[seq];
					float addTime = ((float) contentLength)*8/dnldSpeed - dltime;
					long addTimeNano = ((long)addTime)*1000000000;
					
					Log.i(TAG, "Sleep by " + (long) (addTime * 1000) + "ms");
				    long start = System.nanoTime();
				    Log.i(TAG, "start = " + start);
				    long end = 0;
				    if (addTime >= 0) {
						do{
					        end = System.nanoTime();
					    }while(start + addTimeNano >= end);
				    } else {
				    	dnldSpeed = realBandwidth[seq];
				    }
				}
				
				recordedRawBw = dnldSpeed;
				
				if (IS_BANDWIDTH_SMOOTHENING_ENABLED) {
					if(seq == 0)
						dnldSpeed = (float) dnldSpeed;
					else if (seq == 1)
						dnldSpeed = (float) ((ALPHA+BETA)*prev_bw + (1- ALPHA - BETA) *dnldSpeed);
					else
						dnldSpeed = (float) (ALPHA*prev_prev_bw + BETA*prev_bw + (1- ALPHA - BETA) *dnldSpeed);
				}
				
				recordedSmoothBw = dnldSpeed;
				
				rawBandwidth[seq] = recordedRawBw;
				smoothenBandwidth[seq] = recordedSmoothBw;

				Log.e("spd ", spd + "bytes per seconds");
				Log.e("File Path ", file.getAbsolutePath());
				
				prev_prev_bw = prev_bw;
				prev_bw = dnldSpeed;
				
				return dnldSpeed;
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return 0.0f;
		}

		// Important - this is the part which continue next download and start
		// player
		protected void onPostExecute(Float result) {
			seq++;
			curr_buff_usage++;
			
			bw = result;
			
			updateSegmentInfo();
			
			if(totalSegment == 1){
				// no more files to download	
				if (startPlay == true) {
					startPlay = false;
					playVideo(videoPath.get(0));
				}
			}
			
			if (seq >= BUFFER_MIN || seq == totalSegment) {	
				if (startPlay == true) {
					startPlay = false;
					playVideo(videoPath.get(0));
				} else {
					// start playing again if player has stopped due to depleted buffer
					if (isBuffering && seq > currentVideo) {
						isBuffering = false;
						Log.i(TAG, "seq: " + seq + "currentVideo: " + currentVideo + ", videoPath.size()=" + videoPath.size());
						playVideo(videoPath.get(currentVideo));
					}
				}
			}

			if (seq >= (urls_hi.size())) {
				System.out.println("End Downloading");
				return;
			}
			
			downloadNextSegmentAdaptively();
			
			return;
		}

	}

	private class DownloadXmlTask extends AsyncTask<String, Void, String> {

		@Override
		protected void onPreExecute() {    
			showProgressDialog();
		}

		@Override
		protected String doInBackground(String... urls) {
			try {
				System.out.println("start");
				InputStream stream = downloadUrl(urls[0]);
				DocumentBuilderFactory dbf = DocumentBuilderFactory
						.newInstance();
				DocumentBuilder db = dbf.newDocumentBuilder();
				Document document = db.parse(stream);
				org.w3c.dom.Element element = document.getDocumentElement();
				NodeList list = element.getElementsByTagName("SegmentURL");
				// Parse for bandwidth in MPD
				NodeList list1 = element.getElementsByTagName("Representation");
				
				NamedNodeMap map1 = list1.item(2).getAttributes();
				String v1 = map1.getNamedItem("bandwidth").getNodeValue();
				rate_lo = Integer.parseInt(v1);
				Log.e(TAG,"lo : " + rate_lo);
				
				map1 = list1.item(1).getAttributes();
				v1 = map1.getNamedItem("bandwidth").getNodeValue();
				rate_med = Integer.parseInt(v1);
				Log.e(TAG,"med : " + rate_med);
				
				map1 = list1.item(0).getAttributes();
				v1 = map1.getNamedItem("bandwidth").getNodeValue();
				rate_hi = Integer.parseInt(v1);
				Log.e(TAG,"Hi : " + rate_hi);

				for (int i = 0; i < list.getLength(); i++) {
					Node n = list.item(i);
					NamedNodeMap map = n.getAttributes();
					String value = map.getNamedItem("media").getNodeValue();
					hyperlinks.add(value);
				}

				totalSegment = hyperlinks.size() / 3;
				System.out.println("Total Segment Count   "+totalSegment);
				
				rawBandwidth = new float[totalSegment];
				smoothenBandwidth = new float[totalSegment];
				currQuality = new float[totalSegment];
				buff_usage = new int[totalSegment];
				realBandwidth = new float[totalSegment];
				
				inputBandwidth = new float[totalSegment];
				BufferedReader in;
				
				if (IS_SIMULATED_INPUT_ENABLED) {
					try {
						in = new BufferedReader(new InputStreamReader(getAssets().open(inputBwFilePath)));
					    String tempStr;
					    int i = 0;
					    while ((tempStr = in.readLine()) != null) {
					    	if (i >= totalSegment) {
					    		break;
					    	}
					    	inputBandwidth[i++] = Float.parseFloat(tempStr);
					    }
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
				int size = list.getLength();
				size = size / 3;
				urls_lo.clear();
				urls_med.clear();
				urls_hi.clear();
				for (int i = 0; i < size; i++) {
					urls_hi.add(hyperlinks.get(i));
					urls_med.add(hyperlinks.get(size + i));
					urls_lo.add(hyperlinks.get((size * 2) + i));
				}
				return "";
			} catch (IOException e) {
				e.printStackTrace();
			} catch (ParserConfigurationException e) {
				e.printStackTrace();
			} catch (SAXException e) {
				e.printStackTrace();
			}
			return "";
		}

		@Override
		protected void onPostExecute(String result) {
			isMpdParsed = true;
			seq = 0;
			
			Log.i(TAG, "Download first segment");
			
			new DownloadSegmentTask().execute(urls_lo.get(seq));
			
			segmentQuality.add("LOW quality (" + String.format("%.02f", ((float)rate_lo/(1000*1000))) + " Mbps)");
			
			Log.e("Seg dl " + seq, "low");
		}

		private InputStream downloadUrl(String urlString) throws IOException {
			URL url = new URL(urlString);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setReadTimeout(30000 /* milliseconds */);
			conn.setConnectTimeout(30000 /* milliseconds */);
			conn.setRequestMethod("GET");
			conn.setDoInput(true);
			conn.connect();
			InputStream stream = conn.getInputStream();
			return stream;
		}
	}
	
	public void downloadNextSegmentAdaptively() {
		int currentQuality = 2;
		
		buff_usage[seq-1] = curr_buff_usage;
        Log.i(TAG, "buff_usage["+seq+"-1] = " + buff_usage[seq-1]);
		
		if (IS_USING_BANDWIDTH_ESTIMATION) {
    		if (bw >= (float)rate_hi) {
    			currentQuality = 3;
    		} else if (bw >= (float)rate_med) {
    			currentQuality = 2;
    		} else {
    			currentQuality = 1;
    		}
		} else {
		    currentQuality = 2;
		}
		
		if (IS_BUFFER_THRESHOLDING_ENABLED) {
			if (buff_usage[seq-1] >= MED_TH && buff_usage[seq-1] < BUFFER_MAX) {
				currentQuality = Math.min(currentQuality+1, 3);
			} else if (buff_usage[seq-1] >= BUFFER_MIN && buff_usage[seq-1] < MED_TH) {
				currentQuality = Math.max(currentQuality, 2);
			} else if (buff_usage[seq-1] < BUFFER_MIN){
				currentQuality = Math.min(currentQuality, 1);
			}
		}
		
		if (buff_usage[seq-1] >= BUFFER_MAX) {
			;
		} else if (currentQuality == 3) {
			new DownloadSegmentTask().execute(urls_hi.get(seq));
			segmentQuality.add("HI quality (" + String.format("%.02f", ((float)rate_hi/(1000*1000))) + " MBps)");
			Log.e("Segment dl " + seq, "Hi");
		} else if (currentQuality == 2) {
			new DownloadSegmentTask().execute(urls_med.get(seq));
			segmentQuality.add("MED quality (" + String.format("%.02f", ((float)rate_med/(1000*1000))) + " MBps)");
			Log.e("Segment dl " + seq, "med");
		} else if (currentQuality == 1){
			new DownloadSegmentTask().execute(urls_lo.get(seq));
			segmentQuality.add("LOW quality (" + String.format("%.02f", ((float)rate_lo/(1000*1000))) + " MBps)");
			Log.e("Segment dl " + seq, "Low");
		}
		
		return;
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
		// TODO Auto-generated method stub

	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		if (!isMpdParsed) {
			new DownloadXmlTask().execute(mpdUrl); 
		}
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onCompletion(MediaPlayer mp) {   // completed playing the segment, start the next segment
		Log.d("player", "playback complete   " + currentVideo);
		updateSegmentInfo();
		currentVideo++;
		curr_buff_usage--;	

		Log.i(TAG, "curr_buff_usage = " + curr_buff_usage);
		if (seq < (urls_hi.size()) && curr_buff_usage == BUFFER_MAX -1) {
			downloadNextSegmentAdaptively();
		}
		
		if (currentVideo > videoPath.size() - 1) {
			if (currentVideo < totalSegment-1) {
				showProgressDialog();
				if (mediaPlayer != null) {
					if(mediaPlayer.isPlaying()) {
						mediaPlayer.stop();
					}
					mediaPlayer.reset();
					mediaPlayer.release();
					mediaPlayer = null;
				}
				isBuffering = true;
			} else {			
				System.out.println("Finished Playing");

				endPlayTimeMs = System.currentTimeMillis();
				Log.i("VLCbench:", "Total play time = " + (endPlayTimeMs - startPlayTimeMs) + "ms");
				
				Intent myIntent = new Intent(PlayerActivity.this, MainActivity.class);
				PlayerActivity.this.startActivity(myIntent);
			}
		}
		else
		{
			playVideo(videoPath.get(currentVideo));   // play the next segment
		}
	}

	private void playVideo(
			String videoPath) {
		hideProgressDialog();

		int quality = 0;
		if (segmentQuality.get(currentVideo).startsWith("LOW")) {
			quality = 0;
		} else if (segmentQuality.get(currentVideo).startsWith("MED")) {
			quality = 1;
		} else if (segmentQuality.get(currentVideo).startsWith("HI")) {
			quality = 2;
		}
		
		currQuality[currentVideo] = quality;

		float [] temp = {200.0f*1000.0f,768.0f*1000.0f,3.0f*1000*1000};
		
		if(currentVideo == 0)
			Log.i("VLCbench:", 0 + "," + 0 + "," + 0 + "," + temp[quality]
					+",0");
		else
			Log.i("VLCbench:", realBandwidth[currentVideo-1]+","+rawBandwidth[currentVideo-1] + "," + smoothenBandwidth[currentVideo-1] + "," + temp[quality]
				+"," + buff_usage[currentVideo-1]);
		
		try {
			// free the previous player
			if (mediaPlayer != null) {
				if(mediaPlayer.isPlaying()) {
					mediaPlayer.stop();
				}
				mediaPlayer.reset();
				mediaPlayer.release();
				mediaPlayer = null;
			}
			
			// preparing media player and surface to play videos
			mediaPlayer = new MediaPlayer();
			mediaPlayer.setOnCompletionListener(this);
			mediaPlayer.setDisplay(holder);
			mediaPlayer.setDataSource(videoPath);
			mediaPlayer.prepare();
			
	        mController.setMediaPlayer(this);
	        mController.setAnchorView(findViewById(R.id.surfaceView));
	        handler.post(new Runnable() {

	            public void run() {
	                mController.setEnabled(true);
	            }
	        });
			
	        updateVidQualityInfo();
	        updateSmoothenedBwInfo();

			mediaPlayer.start();    // start playing the videos
			
			if (currentVideo == 0) {
				startPlayTimeMs = System.currentTimeMillis();
			}

		} catch (IllegalArgumentException e) {
			Log.d("MEDIA_PLAYER", e.getMessage());
		} catch (IllegalStateException e) {
			Log.d("MEDIA_PLAYER", e.getMessage());
		} catch (IOException e) {
			Log.d("MEDIA_PLAYER", e.getMessage());
		}

	}

	@Override
	public void onPause() {
		super.onPause();
		if (mediaPlayer != null) {
			mediaPlayer.pause();
		}
	}
	
	@Override
	public void onResume() {
		super.onResume();
		mController.show();
	}
	
	@Override
	protected void onStop() {
		super.onStop();
		if (mediaPlayer != null) {
			mController.hide();
			mediaPlayer.stop();
			mediaPlayer.release();
			mediaPlayer = null;
		}
	}
	
	@Override
	protected void onRestart() {
		super.onRestart();
		if (mediaPlayer != null) {
			playVideo(videoPath.get(currentVideo));
		}
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		//the MediaController will hide after 3 seconds - tap the screen to make it appear again
		mController.show();
		return false;
	}

	@Override
	public boolean canPause() {
		return true;
	}

	@Override
	public boolean canSeekBackward() {
		return true;
	}

	@Override
	public boolean canSeekForward() {
		return true;
	}

	@Override
	public int getAudioSessionId() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getBufferPercentage() {
		return 0;
	}

	@Override
	public int getCurrentPosition() {
		if (mediaPlayer == null) {
			return 0;
		}
        return mediaPlayer.getCurrentPosition();
	}

	@Override
	public int getDuration() {
		if (mediaPlayer == null) {
			return 0;
		}
        return mediaPlayer.getDuration();
	}

	@Override
	public boolean isPlaying() {
		if (mediaPlayer == null) {
			return false;
		}
        return mediaPlayer.isPlaying(); 
	}

	@Override
	public void pause() {
        mediaPlayer.pause();
	}

	@Override
	public void seekTo(int pos) {
        mediaPlayer.seekTo(pos);
	}

	@Override
	public void start() {
        mediaPlayer.start();
	} 

}
