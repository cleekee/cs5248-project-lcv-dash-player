package edu.nus.cs5248;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ListFragment;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.ImageView.ScaleType;
import android.widget.TextView;

public class SubscribeFragment extends ListFragment {
	private static final String TAG = "SubscribeFragment";

	Context context = null;
	
	VideoGalleryAdapter vga;

	//define source of MediaStore.Images.Media, internal or external storage
	private final static Uri sourceUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
	private final static String VIDEO_ID = MediaStore.Video.Media._ID;
	private final static String VIDEO_DATA = MediaStore.Video.Media.DATA;
	private final static String VIDEO_DURATION = MediaStore.Video.Media.DURATION;
	
	private int[] videoIds;
	private String[] videoNames;
	private String[] videoDuration;
	private String[] videoUploadStatus;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {		
		context = this.getActivity();
		
		initvideoIds();

		vga = new VideoGalleryAdapter(context);

		setListAdapter(vga);  

		return super.onCreateView(inflater, container, savedInstanceState);
	}
	
	@Override
	public void onActivityCreated(Bundle saved) { 
		super.onActivityCreated(saved);
		
		this.getListView().setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				// Now we want to actually get the data location of the file
				String [] proj={VIDEO_DATA};
				// We request our cursor again
				Cursor _cursor = SubscribeFragment.this.getActivity().getContentResolver().query(sourceUri,
						proj, // Which columns to return
						null,//VIDEO_DATA + " like ? ",       // WHERE clause; which rows to return (all rows)
						null,//new String[] {"%Movies%"},       // WHERE clause selection arguments (none)
						null); // Order-by clause (ascending by name)

				_cursor.moveToFirst();

				// We want to get the column index for the data uri
				int columnIndex = _cursor.getColumnIndex(VIDEO_DATA);
				// Lets move to the selected item in the cursor
				_cursor.moveToPosition(position);
				// And here we get the filename
				String filename = _cursor.getString(columnIndex);
				
				File file = new File(filename);

			    Intent intent = new Intent();
			    intent.setAction(Intent.ACTION_VIEW);
				intent.setDataAndType(Uri.fromFile(file), "video/*");
			    
			    startActivity(intent);
			}
			
		});

		registerForContextMenu(this.getListView());
	}
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
	    ContextMenuInfo menuInfo) {
      super.onCreateContextMenu(menu, v, menuInfo);
	  if (v.getId()==this.getListView().getId()) {
	    AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)menuInfo;
	    menu.setHeaderTitle("Actions");
	    String[] menuItems = {"Upload", "Delete"};
	    for (int i = 0; i<menuItems.length; i++) {
	      menu.add(Menu.NONE, i, i, menuItems[i]);
	    }
	  }
	}
	
    @Override
    public boolean onContextItemSelected(MenuItem item) {
    	AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
        switch (item.getItemId()) {
            case 0:
                uploadVideo(info.position);
                return true;
            case 1:
                Toast.makeText(getActivity(), "Deleting", Toast.LENGTH_SHORT).show();
                deleteVideo(info.position);
            	((MainActivity)SubscribeFragment.this.getActivity()).reloadFragment();
                return true;
        }
        return super.onContextItemSelected(item);
    }
    
    public void uploadVideo(int position) {
		// Now we want to actually get the data location of the file
		String [] proj={VIDEO_DATA};
		// We request our cursor again
		Cursor _cursor = SubscribeFragment.this.getActivity().getContentResolver().query(sourceUri,
				proj, // Which columns to return
				null,//VIDEO_DATA + " like ? ",       // WHERE clause; which rows to return (all rows)
				null,//new String[] {"%Movies%"},       // WHERE clause selection arguments (none)
				null); // Order-by clause (ascending by name)

		_cursor.moveToFirst();

		// We want to get the column index for the data uri
		int columnIndex = _cursor.getColumnIndex(VIDEO_DATA);
		// Lets move to the selected item in the cursor
		_cursor.moveToPosition(position);
		// And here we get the filename
		String filename = _cursor.getString(columnIndex);
		//*********** You can do anything when you know the file path :-)

		Shorten segmentTask = new Shorten(SubscribeFragment.this.getActivity(), 
				SubscribeFragment.this, position, filename, videoDuration[position]);
		segmentTask.execute();
    }
    
    public void deleteVideo(int position) {
    	// Now we want to actually get the data location of the file
    	String [] proj={VIDEO_ID};
    	// We request our cursor again
    	Cursor _cursor = SubscribeFragment.this.getActivity().getContentResolver().query(sourceUri,
    			proj, // Which columns to return
    			null,//VIDEO_DATA + " like ? ",       // WHERE clause; which rows to return (all rows)
    			null,//new String[] {"%Movies%"},       // WHERE clause selection arguments (none)
    			null); // Order-by clause (ascending by name)

    	_cursor.moveToFirst();

    	int idIndex = _cursor.getColumnIndex(VIDEO_ID);
    	// Lets move to the selected item in the cursor
    	_cursor.moveToPosition(position);
    	// And here we get the video ID
    	int videoId = _cursor.getInt(idIndex);
    	
    	//*********** You can do anything when you know the file path :-)
    	Uri uri = ContentUris.withAppendedId(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, 
    			videoId);
    	context.getContentResolver().delete(uri, null, null);
    }
	
	public BaseAdapter getAdapter() {
		return vga;
	}
	
	public void updateVideoStatus(int i, String value) {
		videoUploadStatus[i] = value;//getUploadStatus(videoNames[i]);
	}

	private void initvideoIds() {
		Log.i(TAG, "creating cursor... with context " + this.getActivity());

		//Here we set up a string array of the thumbnail ID column we want to get back
		String [] proj={VIDEO_ID, VIDEO_DATA,VIDEO_DURATION};

		// Now we create the cursor pointing to the external thumbnail store
		Cursor _cursor = this.getActivity().getContentResolver().query(sourceUri,
				proj, // Which columns to return
				null,//VIDEO_DATA + " like ? ",       // WHERE clause; which rows to return (all rows)
				null,//new String[] {"%Movies%"},       // Filter by folder
				null); // Order-by clause (ascending by name)

		int count = _cursor.getCount();

		// We now get the column index of the thumbnail id
		int columnIndex = _cursor.getColumnIndex(VIDEO_ID);
		int nameIndex = _cursor.getColumnIndex(VIDEO_DATA);
		int durationIndex = _cursor.getColumnIndex(VIDEO_DURATION);
		
		//initialize 
		videoIds = new int[count];
		videoNames = new String[count];
		videoDuration = new String[count];
		videoUploadStatus = new String[count];
		
		//move position to first element
		_cursor.moveToFirst();          
		for(int i=0;i<count;i++)
		{           
			int id = _cursor.getInt(columnIndex);

			videoIds[i]= id;
			videoNames[i]= _cursor.getString(nameIndex);
			int time_in_msec = Integer.parseInt(_cursor.getString(durationIndex));
			int msec = time_in_msec%1000;
			time_in_msec = time_in_msec/1000;
			int sec = time_in_msec%60;
			time_in_msec /= 60;
			int min = time_in_msec%60;
			time_in_msec /= 60;
			int hr = time_in_msec;
			
			String time_in_correct_format = String.format("%d:%02d:%02d.%03d", hr,min,sec,msec);

			videoDuration[i] = time_in_correct_format;//_cursor.getString(durationIndex);
			
			int myCount = 0;
			int myPosition = 0;
			for(int j=videoNames[i].length()-1;j>=0;j--)
			{
				if(videoNames[i].charAt(j)=='/')
					myCount++;
				if(myCount == 2)
				{	myPosition = j;
					break;
				}
			}

			videoNames[i] = videoNames[i].substring(myPosition+1);
            
            new GetUploadStatusTask(i).execute();
            
			_cursor.moveToNext();

		}
		
		if( _cursor != null && !_cursor.isClosed() ) {
			_cursor.close();
		}
	}
	
	private class VideoGalleryAdapter extends BaseAdapter
	{
		Context context = null;
		
		public VideoGalleryAdapter(Context c) 
		{
			context = c;
		}
		public int getCount() 
		{
			return videoIds.length;
		}
		public Object getItem(int position) 
		{
			return position;
		}
		public long getItemId(int position) 
		{
			return position;
		}
		@Override
		public View getView(final int position, View convertView, ViewGroup parent) 
		{
	        ViewHolder viewHolder;
	        
	        if(convertView == null) {
	            // inflate the GridView item layout
	            LayoutInflater inflater = LayoutInflater.from(getActivity());
	            convertView = inflater.inflate(R.layout.video_list_item, parent, false);
	            
	            // initialize the view holder
	            viewHolder = new ViewHolder();
	            viewHolder.ivIcon = (ImageView) convertView.findViewById(R.id.thumbnail);
	            viewHolder.tvTitle = (TextView) convertView.findViewById(R.id.video_name);
	            viewHolder.tvDuration = (TextView) convertView.findViewById(R.id.video_durn);
	            viewHolder.tvDescription = (TextView) convertView.findViewById(R.id.upload_status);            
	            
	            convertView.setTag(viewHolder);
	        } else {
	            // recycle the already inflated view 
	            viewHolder = (ViewHolder) convertView.getTag();
	        }
	        
	        //After recycling has been done, viewHolder is available to use

	        viewHolder.ivIcon.setImageBitmap(getImage(videoIds[position]));
	        viewHolder.ivIcon.setScaleType(ScaleType.FIT_XY);
            viewHolder.tvTitle.setText(videoNames[position]);
            viewHolder.tvDuration.setText("Duration: " + videoDuration[position]);
            viewHolder.tvDescription.setText(videoUploadStatus[position]);
            
	        return convertView;

		}
		
		private class ViewHolder {
	        ImageView ivIcon;
	        TextView tvTitle;
	        TextView tvDuration;
	        TextView tvDescription;
	    }

		// Create the thumbnail on the fly
		private Bitmap getImage(int id) {
			Bitmap thumb = MediaStore.Video.Thumbnails.getThumbnail(
					context.getContentResolver(),
					id, MediaStore.Video.Thumbnails.MICRO_KIND, null);
			return thumb;
		}
	}
	
	public class ListViewItem {
	    public final Drawable icon;       // the drawable for the ListView item ImageView
	    public final String title;        // the text for the ListView item title
	    public final String duration;
	    public final String description;  // the text for the ListView item description
	    
	    public ListViewItem(Drawable icon, String title, String duration, String description) {
	        this.icon = icon;
	        this.title = title;
	        this.duration = duration;
	        this.description = description;
	    }
	}
	
	private class GetUploadStatusTask extends AsyncTask<String, Integer, String> {
		int position;
		
		public GetUploadStatusTask(int position) {
			this.position = position;
		}
		
		protected String doInBackground(String... urls) {
			return getUploadStatus(videoNames[position]);
		}

		protected void onPostExecute(String result) {
			videoUploadStatus[position] = result;
			vga.notifyDataSetChanged();
		}
	}
	
	public String getUploadStatus(String name)
	{
		name = name.substring(name.lastIndexOf("/")+1).replaceAll(".mp4", "");
		
		// Create a new HttpClient and Post Header
	    HttpClient httpclient = new DefaultHttpClient();
        
	    HttpPost httppost = new HttpPost("http://pilatus.d1.comp.nus.edu.sg/~a0035670/retrieveUpStatus.php");
	    
	    MultipartEntityBuilder builder = MultipartEntityBuilder.create();        
	    builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
	    
	    builder.addTextBody("videoName", name);
	    
	    httppost.setEntity(builder.build());
		HttpResponse response;
        
		String uploadStatus="";
		
        // Execute HTTP Post Request
        try {
			response = httpclient.execute(httppost);
			uploadStatus = getResponseString(response);
			uploadStatus = uploadStatus.substring(1, uploadStatus.length()-2);
			
			if(uploadStatus.isEmpty()) {
				uploadStatus = "Not uploaded";
			} 
			else
			{
				String totSeg,upSeg;
				Log.i(TAG, "uploadStatus: " + uploadStatus);
				JSONObject obj = new JSONObject(uploadStatus);
				totSeg = obj.getString("segTotal");
				upSeg = obj.getString("segUploaded");
				
				if(totSeg.equals(upSeg)) {
					uploadStatus = "Upload complete";
				} else {
					uploadStatus = "Uploading " + upSeg + "/" + totSeg;
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		return uploadStatus;
	}
	
    private String getResponseString(HttpResponse response) {
        HttpEntity entity = response.getEntity();
        InputStream is;
        StringBuilder sb = new StringBuilder();
		try {
			is = entity.getContent();
			
		    //convert response to string
	    	BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
	    	String line = null;
	    	while ((line = reader.readLine()) != null) {
	    		sb.append(line + "\n");
	    	}
	    	is.close();
	    	
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return sb.toString();
    }
	 
}
