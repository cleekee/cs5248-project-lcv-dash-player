package edu.nus.cs5248;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.coremedia.iso.boxes.Container;
import com.googlecode.mp4parser.authoring.Movie;
import com.googlecode.mp4parser.authoring.Track;
import com.googlecode.mp4parser.authoring.builder.DefaultMp4Builder;
import com.googlecode.mp4parser.authoring.container.mp4.MovieCreator;
import com.googlecode.mp4parser.authoring.tracks.AppendTrack;
import com.googlecode.mp4parser.authoring.tracks.CroppedTrack;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.channels.FileChannel;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.DefaultHttpClient;

/**
 * Shortens/Crops a track
 */
public class Shorten extends AsyncTask<String, Integer, Integer> {
	private static final String TAG = "Shorten";
	
	Activity activity;
	SubscribeFragment hostFragment;
	int videoIndex;
	String filename;
	String duration;
	
	public static double DURATION = 3;
	
	public Shorten(Activity activity, SubscribeFragment subscribeFragment,
			int position, String filename, String duration) {
        this.activity = activity;
        this.hostFragment = subscribeFragment;
        this.videoIndex = position;
        this.filename = filename;
        this.duration = duration;
	}
	
    private String getResponseString(HttpResponse response) {
        HttpEntity entity = response.getEntity();
        InputStream is;
        StringBuilder sb = new StringBuilder();
		try {
			is = entity.getContent();
			
		    //convert response to string
	    	BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
	    	String line = null;
	    	while ((line = reader.readLine()) != null) {
	    		sb.append(line + "\n");
	    	}
	    	is.close();
	    	
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return sb.toString();
    }
	
	public boolean getUploadStatus(String name)
	{	
		name = name.substring(name.lastIndexOf("/")+1).replaceAll(".mp4", "");
		
		// Create a new HttpClient and Post Header
	    HttpClient httpclient = new DefaultHttpClient();
        
	    HttpPost httppost = new HttpPost("http://pilatus.d1.comp.nus.edu.sg/~a0035670/retrieveUpStatus.php");
	    
	    MultipartEntityBuilder builder = MultipartEntityBuilder.create();        
	    builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
	    
	    builder.addTextBody("videoName", name);
	    
	    httppost.setEntity(builder.build());
		HttpResponse response;
        
		String uploadStatus="";
		
        // Execute HTTP Post Request
        try {
			response = httpclient.execute(httppost);
			uploadStatus = getResponseString(response);
			uploadStatus = uploadStatus.substring(1, uploadStatus.length()-2);
			
			if(uploadStatus.isEmpty()) {
				return true;
			} 
			else
			{
				return false;
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		return true;
	}
	
	public int startSegment(String videoPath, Context context) {
		int stream_index = 0;
		try {
			Movie movie = MovieCreator.build(videoPath);
	        
	        double videoDuration = 0;
	        for (Track track :  movie.getTracks()) {
	        	if (track.getHandler().equals("vide")) {
	        		videoDuration = (double) (track.getDuration()) / track.getTrackMetaData().getTimescale();
	        		break;
	        	}
	        }

        	Log.i(TAG, "video duration = " + videoDuration);
        	
	        double startTime = 0;
	        double endTime = 0;
	        while ((endTime = shorten(startTime, videoPath,stream_index,context)) < videoDuration) {
	    	    hostFragment.updateVideoStatus(videoIndex, "Segmenting streamlet " + (stream_index+1));
	            publishProgress(stream_index+1);
	            
	        	startTime = endTime;
	        	stream_index++;
	        }
		} catch (IOException e) {
			Log.e(TAG, Log.getStackTraceString(e));
		}
		
		return stream_index+1;
	}
	
    public static double shorten(double startTime1, String videoPath, int stream_idx, Context context) throws IOException {
//        double endTime1 = startTime1 + DURATION;
        double endTime1 = (stream_idx + 1)*DURATION;
    	Log.i(TAG, "Trying to cut segment " + stream_idx + " from " + startTime1 + "s to " + endTime1 + "s");
    	
        boolean timeCorrected = false;
        
		Movie movie = MovieCreator.build(videoPath);

        List<Track> tracks = movie.getTracks();
        // remove all tracks we will create new tracks from the old
        movie.setTracks(new LinkedList<Track>());

        // Here we try to find a track that has sync samples. Since we can only start decoding
        // at such a sample we SHOULD make sure that the start of the new fragment is exactly
        // such a frame
        for (Track track : tracks) {
            if (track.getSyncSamples() != null && track.getSyncSamples().length > 0) {
                if (timeCorrected) {
                    // This exception here could be a false positive in case we have multiple tracks
                    // with sync samples at exactly the same positions. E.g. a single movie containing
                    // multiple qualities of the same video (Microsoft Smooth Streaming file)

                    throw new RuntimeException("The startTime has already been corrected by another track with SyncSample. Not Supported.");
                }
                startTime1 = correctTimeToSyncSample(track, startTime1, false);
                endTime1 = correctTimeToSyncSample(track, endTime1, true);
                timeCorrected = true;
            }
        }

    	Log.i(TAG, "Corrected time: " + startTime1 + "s to " + endTime1 + "s");
    	Log.i("segDuration", "" + (endTime1 - startTime1));

        for (Track track : tracks) {
            long currentSample = 0;
            double currentTime = 0;
            double lastTime = 0;
            long startSample1 = -1;
            long endSample1 = -1;

            for (int i = 0; i < track.getSampleDurations().length; i++) {
                long delta = track.getSampleDurations()[i];


                if (currentTime >= lastTime && currentTime <= startTime1) {
                    // current sample is still before the new starttime
                    startSample1 = currentSample;
                }
                if (currentTime >= lastTime && currentTime <= endTime1) {
                    // current sample is after the new start time and still before the new endtime
                    endSample1 = currentSample;
                }

                lastTime = currentTime;
                currentTime += (double) delta / (double) track.getTrackMetaData().getTimescale();
                currentSample++;
            }
            movie.addTrack(new AppendTrack(new CroppedTrack(track, startSample1, endSample1)));
        }

        Container out = new DefaultMp4Builder().build(movie);
               
        String file_name = videoPath.substring(videoPath.lastIndexOf('/')+1, videoPath.lastIndexOf('.'));
        File tempFile = new File(context.getFilesDir(), String.format("%s_%d.mp4", file_name, stream_idx));

        FileOutputStream fos = new FileOutputStream(tempFile);
        
        FileChannel fc = fos.getChannel();
        out.writeContainer(fc);
        
        fc.close();
        fos.close();
        
        Log.i(TAG, "tempFile.length() = " + tempFile.length());
        
    	Log.i(TAG, "[Shorten] returning: " + endTime1);
    	
        return endTime1;
    }


    private static double correctTimeToSyncSample(Track track, double cutHere, boolean next) {
        double[] timeOfSyncSamples = new double[track.getSyncSamples().length];
        long currentSample = 0;
        double currentTime = 0;
        for (int i = 0; i < track.getSampleDurations().length; i++) {
            long delta = track.getSampleDurations()[i];

            if (Arrays.binarySearch(track.getSyncSamples(), currentSample + 1) >= 0) {
                // samples always start with 1 but we start with zero therefore +1
                timeOfSyncSamples[Arrays.binarySearch(track.getSyncSamples(), currentSample + 1)] = currentTime;
            }
            currentTime += (double) delta / (double) track.getTrackMetaData().getTimescale();
            currentSample++;

        }
        double previous = 0;
        for (double timeOfSyncSample : timeOfSyncSamples) {
            if (timeOfSyncSample > cutHere) {
                if (next) {
                	if ((timeOfSyncSample - cutHere) <= (cutHere - previous)) {
                		return timeOfSyncSample;
                	} else {
                		return previous;
                	}
                } else {
                    return previous;
                }
            }
            previous = timeOfSyncSample;
        }
        
        double trackDuration = (double) (track.getDuration()) / track.getTrackMetaData().getTimescale();
        if (next) {
        	return trackDuration;
        } else {
        	return timeOfSyncSamples[timeOfSyncSamples.length - 1];
        }        
    }

	@Override
	protected Integer doInBackground(String... params) {
		if(getUploadStatus(filename))
			return startSegment(filename, activity);
		else 
			return -1;
	}
	
    @Override
    protected void onProgressUpdate (Integer... values) {
		hostFragment.getAdapter().notifyDataSetChanged();
    }
	
    @Override
    protected void onPostExecute(Integer result) {
		UploadVideoTask uvt = new UploadVideoTask(activity, 
				hostFragment, videoIndex, result, duration);
		uvt.execute(filename);
    }
}