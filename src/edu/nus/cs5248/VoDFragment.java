package edu.nus.cs5248;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Fragment;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class VoDFragment extends Fragment {
	private static final String TAG = "VoDFragment";
	
	MpdAdapter listAdapter;
	ListView lv;
	LinearLayout linlaHeaderProgress;
	
	private List<String> videoNames;
	private List<String> videoDuration;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_vod, container, false);
		
		videoNames = new ArrayList<String>();
		videoDuration = new ArrayList<String>();
		
		// CAST THE LINEARLAYOUT HOLDING THE MAIN PROGRESS (SPINNER)
		linlaHeaderProgress = (LinearLayout) view.findViewById(R.id.linlaHeaderProgress);

		lv = (ListView) view.findViewById(R.id.mainListView);
		listAdapter = new MpdAdapter();
		lv.setAdapter(listAdapter);
		lv.setClickable(true);
		lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {  // user select the mpd file for some video
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int position, long arg3) {				
				ListViewItem lvItem = (ListViewItem) lv.getItemAtPosition(position);    // get the mpd file, selected by user
				
				Intent intent = new Intent(getActivity(), PlayerActivity.class);
				String mpdPath = "http://pilatus.d1.comp.nus.edu.sg/~a0035670/upload/" 
						+ lvItem.title + "/" + lvItem.title + ".mpd";
				
				Log.i(TAG, "mpdPath: " + mpdPath);
				
				intent.putExtra("url", mpdPath);
				intent.putExtra("simulate_file", "");
				startActivity(intent);
			}
		});
		
		registerForContextMenu(lv);

		new Populate().execute(); 
		return view;
	}
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
	    ContextMenuInfo menuInfo) {
      super.onCreateContextMenu(menu, v, menuInfo);
	  if (v.getId()==lv.getId()) {
	    menu.setHeaderTitle("Actions");
	    String[] menuItems = {"Delete" , "Play with simulated bandwidth"};
	    for (int i = 0; i<menuItems.length; i++) {
	      menu.add(Menu.NONE, i, i, menuItems[i]);
	    }
	  }
	}
	
    @Override
    public boolean onContextItemSelected(MenuItem item) {
    	AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
        switch (item.getItemId()) {
            case 0:
                Toast.makeText(getActivity(), "Delete", Toast.LENGTH_SHORT).show();
                
				ListViewItem lvItem = (ListViewItem) lv.getItemAtPosition(info.position);
			    new DeleteVideoTask().execute(lvItem.title);
			    
                return true;
            case 1:
				ListViewItem lvItem2 = (ListViewItem) lv.getItemAtPosition(info.position);

				Intent intent = new Intent(getActivity(), PlayerActivity.class);
				String mpdPath = "http://pilatus.d1.comp.nus.edu.sg/~a0035670/upload/" 
						+ lvItem2.title + "/" + lvItem2.title + ".mpd";
				Log.i("CLK", "mpdPath: " + mpdPath);
				intent.putExtra("url", mpdPath);
				intent.putExtra("simulate_file", "real_world_bw");
				startActivity(intent);
			    
                return true;
        }
        return super.onContextItemSelected(item);
    }
	
	public String getMpdList()
	{
		// Create a new HttpClient and Post Header
	    HttpClient httpclient = new DefaultHttpClient();
        
	    HttpPost httppost = new HttpPost("http://pilatus.d1.comp.nus.edu.sg/~a0035670/retrieveUpVideos.php");
	    
	    MultipartEntityBuilder builder = MultipartEntityBuilder.create();        
	    builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);	    
	    httppost.setEntity(builder.build());
		HttpResponse response;
        
		String jsonString = "";
		
        // Execute HTTP Post Request
        try {
			response = httpclient.execute(httppost);
			jsonString = getResponseString(response);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		return jsonString;
	}
	
    private String getResponseString(HttpResponse response) {
        HttpEntity entity = response.getEntity();
        InputStream is;
        StringBuilder sb = new StringBuilder();
		try {
			is = entity.getContent();
			
		    //convert response to string
	    	BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
	    	String line = null;
	    	while ((line = reader.readLine()) != null) {
	    		sb.append(line + "\n");
	    	}
	    	is.close();
	    	
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return sb.toString();
    }
    
    private class DeleteVideoTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
        	HttpClient httpclient = new DefaultHttpClient();
	        
		    HttpPost httppost = new HttpPost("http://pilatus.d1.comp.nus.edu.sg/~a0035670/deleteUpVideo.php");
		    
		    MultipartEntityBuilder builder = MultipartEntityBuilder.create();        
		    builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);	    

		    builder.addTextBody("videoName", urls[0]);
		    
		    httppost.setEntity(builder.build());
			
	        // Execute HTTP Post Request
	        try {
				httpclient.execute(httppost);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			return null;
        }
        
        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
        	((MainActivity)VoDFragment.this.getActivity()).reloadFragment();
       }
    }

	private class Populate extends AsyncTask<String, Void, String> {

		@Override
		protected void onPreExecute() {    
		    // SHOW THE SPINNER WHILE LOADING FEEDS
			linlaHeaderProgress.setVisibility(View.VISIBLE);
		}
		
		@Override
		protected String doInBackground(String... urls) {
			return getMpdList();
		}

		@Override
		protected void onPostExecute(String result) {		
			JSONArray jsonArray;
			try {
				jsonArray = new JSONArray(result);

				for(int i=0; i<jsonArray.length(); i++){
					JSONObject jsonData = jsonArray.getJSONObject(i);
					videoNames.add(jsonData.getString("Name"));
					videoDuration.add(jsonData.getString("Duration"));
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		    // HIDE THE SPINNER AFTER LOADING FEEDS
			linlaHeaderProgress.setVisibility(View.GONE);
			
			listAdapter.notifyDataSetChanged();
		}
	}
	
	private class MpdAdapter extends BaseAdapter {
		@Override
		public int getCount() {
			return videoNames.size();
		}

		@Override
		public ListViewItem getItem(int position) {
			return new ListViewItem(videoNames.get(position), videoDuration.get(position));
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
	        ViewHolder viewHolder;
	        
	        if(convertView == null) {
	            // inflate the GridView item layout
	            LayoutInflater inflater = LayoutInflater.from(getActivity());
	            convertView = inflater.inflate(R.layout.mpd_list_item, parent, false);
	            
	            // initialize the view holder
	            viewHolder = new ViewHolder();
	            viewHolder.tvTitle = (TextView) convertView.findViewById(R.id.video_name);
	            viewHolder.tvDuration = (TextView) convertView.findViewById(R.id.video_durn);   
	            
	            convertView.setTag(viewHolder);
	        } else {
	            // recycle the already inflated view 
	            viewHolder = (ViewHolder) convertView.getTag();
	        }
	        
	        //After recycling has been done, viewHolder is available to use

            viewHolder.tvTitle.setText(videoNames.get(position));
            viewHolder.tvDuration.setText("Duration: " + videoDuration.get(position));
            
	        return convertView;
		}
		
		private class ViewHolder {
	        TextView tvTitle;
	        TextView tvDuration;
	    }
	}
	
	public class ListViewItem {
	    public final String title;        
	    public final String duration;
	    
	    public ListViewItem(String title, String duration) {
	        this.title = title;
	        this.duration = duration;
	    }
	}
}
