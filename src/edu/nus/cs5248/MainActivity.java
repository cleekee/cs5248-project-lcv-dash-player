package edu.nus.cs5248;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

public class MainActivity extends Activity implements ActionBar.TabListener {
	static final int REQUEST_VIDEO_CAPTURE = 1;
	
	Fragment mSubscribeFragment = new SubscribeFragment();
	Fragment mVoDFragment = new VoDFragment();

	Fragment fragmentToShow = null;
	Fragment fragmentToHide = null;

	private void recordVideo() {
	    Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
	    if (takeVideoIntent.resolveActivity(getPackageManager()) != null) {
	        startActivityForResult(takeVideoIntent, REQUEST_VIDEO_CAPTURE);
	    }
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_main);

		setupTabs();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		switch (id) {
		case R.id.action_refresh:
			reloadFragment();
			return true;
		case R.id.action_record:
			recordVideo();
			return true;
		case R.id.action_settings:
			return true;
		default:
		}
		return super.onOptionsItemSelected(item);
	}

	public void reloadFragment() {
		Toast.makeText(this, "Refreshing", Toast.LENGTH_SHORT).show();
		
		Fragment frg = fragmentToShow;
		final FragmentTransaction ft = getFragmentManager().beginTransaction();
		ft.detach(frg);
		ft.attach(frg);
		ft.commit();
	}

	private void setupTabs() {
		// Set up the action bar to show tabs.
		final ActionBar actionBar = getActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

		// for each of the sections in the app, add a tab to the action bar.
		actionBar.addTab(actionBar.newTab().setText("Local")
				.setTabListener(this));
		actionBar.addTab(actionBar.newTab().setText("Uploaded")
				.setTabListener(this));
	}

	@Override
	public void onTabReselected(Tab tab, FragmentTransaction ft) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTabSelected(Tab tab, FragmentTransaction ft) {
		// When the given tab is selected, show the tab contents in the
		// container view.
		if (tab.getPosition() == 0) {
			fragmentToShow = mSubscribeFragment;
			fragmentToHide = mVoDFragment;
		} else if (tab.getPosition() == 1) {
			fragmentToShow = mVoDFragment;
			fragmentToHide = mSubscribeFragment;
		}
		FragmentTransaction ftransact = getFragmentManager().beginTransaction();
		
//		ftransact.replace(R.id.container, fragment).commit();
	    
	    if (fragmentToShow.isAdded()) { // if the fragment is already in container
	    	ftransact.show(fragmentToShow);
	    } else { // fragment needs to be added to frame container
	    	ftransact.add(R.id.container, fragmentToShow);
	    }
	    // Hide fragment B
	    if (fragmentToHide.isAdded()) { ftransact.hide(fragmentToHide); }
	    // Commit changes
	    ftransact.commit();
	}

	@Override
	public void onTabUnselected(Tab tab, FragmentTransaction ft) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		
		reloadFragment();
	} 
}
