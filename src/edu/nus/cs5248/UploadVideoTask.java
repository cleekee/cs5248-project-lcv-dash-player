package edu.nus.cs5248;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

class UploadVideoTask extends AsyncTask<String, Integer, Long> {
	private static final String TAG = "UploadVideoTask";
	
	Activity activity;
	SubscribeFragment hostFragment;
	int videoIndex;
	int numSegments;
	String duration;
	
	HttpResponse response;

	/** The Progress dialog. */

    public UploadVideoTask(Activity a, SubscribeFragment hostFragment, int videoIndex, int numSegments, String duration)
    {
        this.activity = a;
        this.hostFragment = hostFragment;
        this.videoIndex = videoIndex;
        this.numSegments = numSegments;
        this.duration = duration;
    }
    
    /**
     * Set the Progress dialog.
     */
    @Override
    protected void onPreExecute() 
    {
    	super.onPreExecute();
    }

	public int getUploadStatus(String name)
	{	
		name = name.substring(name.lastIndexOf("/")+1).replaceAll(".mp4", "");
		
		// Create a new HttpClient and Post Header
	    HttpClient httpclient = new DefaultHttpClient();
        
	    HttpPost httppost = new HttpPost("http://pilatus.d1.comp.nus.edu.sg/~a0035670/retrieveUpStatus.php");
	    
	    MultipartEntityBuilder builder = MultipartEntityBuilder.create();        
	    builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
	    
	    builder.addTextBody("videoName", name);
	    
	    httppost.setEntity(builder.build());
		HttpResponse response;
        
		String uploadStatus="";
		
        // Execute HTTP Post Request
        try {
			response = httpclient.execute(httppost);
			uploadStatus = getResponseString(response);
			uploadStatus = uploadStatus.substring(1, uploadStatus.length()-2);
			
			if(uploadStatus.isEmpty()) {
				return 0;
			} 
			else
			{
				String totSeg,upSeg;
				Log.i(TAG, "uploadStatus: " + uploadStatus);
				JSONObject obj = new JSONObject(uploadStatus);
				totSeg = obj.getString("segTotal");
				upSeg = obj.getString("segUploaded");
				
				if(totSeg.equals(upSeg)) {
					return Integer.parseInt(totSeg);
				} else {
					numSegments = Integer.parseInt(totSeg);
					return Integer.parseInt(upSeg);
				}
			}
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		return -1;
	}

    
    @Override
    protected Long doInBackground(String... urls) {
    	String filename = urls[0];
    	
    	int start_segment = getUploadStatus(filename);
    	
		for (int i=start_segment; i<numSegments;i++) {
    	    hostFragment.updateVideoStatus(videoIndex, "Uploading " + (i+1) + "/" + numSegments);
            publishProgress(i+1);

            String segmentName = activity.getFilesDir().getPath() 
            		+ filename.substring(filename.lastIndexOf('/'), filename.indexOf(".mp4")) 
            		+ "_" + i + ".mp4";
            			
			// Create a new HttpClient and Post Header
		    HttpClient httpclient = new DefaultHttpClient();

		    try {
		    	
		    	if(i == 0)
		    	{
				    HttpPost httppost = new HttpPost("http://pilatus.d1.comp.nus.edu.sg/~a0035670/uploadv2.php");
				    
				    MultipartEntityBuilder builder = MultipartEntityBuilder.create();        
				    builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
				    File file = new File(segmentName);
	
				    builder.addBinaryBody("file", file, ContentType.create("video/mp4"), file.getName());
				    Log.i(TAG, "isFile = " + file.isFile());
				    
				    WifiManager wifiManager = (WifiManager) activity.getSystemService(Context.WIFI_SERVICE);
				    WifiInfo wInfo = wifiManager.getConnectionInfo();
				    // We can attach MAC address of the device to the file name 
				    // to identify files from different devices. Not used at the moment.
				    String mac = wInfo.getMacAddress();
				    
				    String name = file.getAbsolutePath();
				    name = name.substring(name.lastIndexOf('/')+1, name.lastIndexOf('_'));
				    
				    builder.addTextBody("name", name);
				    builder.addTextBody("duration", duration);
				    builder.addTextBody("segTotal", Integer.toString(numSegments));
				    builder.addTextBody("segUploaded", "1");
				    builder.addTextBody("initFile", "0");
				    
				    httppost.setEntity(builder.build());
				    
				    Log.i(TAG, "Sending POST message (name: " + name + ", duration:" + duration 
				    		+ ", segTotal:" + numSegments + ")");
		            
			        // Execute HTTP Post Request
			        response = httpclient.execute(httppost);
		    	}
		    	
		    	else
		    	{
				    HttpPost httppost = new HttpPost("http://pilatus.d1.comp.nus.edu.sg/~a0035670/uploadv2.php");
				    
				    MultipartEntityBuilder builder = MultipartEntityBuilder.create();        
				    builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
				    File file = new File(segmentName);
	
				    builder.addBinaryBody("file", file, ContentType.create("video/mp4"), file.getName());
				    Log.i(TAG, "isFile = " + file.isFile());
				    
				    WifiManager wifiManager = (WifiManager) activity.getSystemService(Context.WIFI_SERVICE);
				    WifiInfo wInfo = wifiManager.getConnectionInfo();
				    String mac = wInfo.getMacAddress();
				    
				    String name = file.getAbsolutePath();
				    name = name.substring(name.lastIndexOf('/')+1, name.lastIndexOf('_'));
				    
				    builder.addTextBody("name", name);
				    builder.addTextBody("segUploaded", Integer.toString(i+1));
				    builder.addTextBody("initFile", "0");
				    
				    httppost.setEntity(builder.build());
		            
				    Log.i(TAG, "Sending POST message (name: " + name + ", segUploaded:" 
				    		+ Integer.toString(i+1) + ")");
		            
			        // Execute HTTP Post Request
			        response = httpclient.execute(httppost);		    		
		    	}

		        Log.i(TAG, ""+response.getStatusLine());
		        Log.i(TAG, "HTTP response: " + getResponseString(response));
		    	
	    	    try {
	    	        // delete the original file
	    	        new File(segmentName).delete();  
	    	    }
	    	   catch (Exception e) {
	    	        Log.e("tag", e.getMessage());
	    	    }

		    } catch (ClientProtocolException e) {
		        // TODO Auto-generated catch block
		    } catch (IOException e) {
		        // TODO Auto-generated catch block
		    } catch(Exception e) {
		    	Log.e("log_tag", "Error converting result "+e.toString());
		    } 
		}
		
		return null;
    }
    
    @Override
    protected void onProgressUpdate (Integer... values) {
		hostFragment.getAdapter().notifyDataSetChanged();
    }

    @Override
    protected void onPostExecute(Long result) {
    	hostFragment.updateVideoStatus(videoIndex, "Upload complete");
		hostFragment.getAdapter().notifyDataSetChanged();
    }
    
    private String getResponseString(HttpResponse response) {
        HttpEntity entity = response.getEntity();
        InputStream is;
        StringBuilder sb = new StringBuilder();
		try {
			is = entity.getContent();
			
		    //convert response to string
	    	BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
	    	String line = null;
	    	while ((line = reader.readLine()) != null) {
	    		sb.append(line + "\n");
	    	}
	    	is.close();
	    	
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return sb.toString();
    }
}
