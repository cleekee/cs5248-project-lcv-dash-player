# CS5248 project #
## LCV DASH: A Video on Demand DASH player for Android devices ##

Our project attempts to build a simple client-server-based video hosting and streaming service based on MPEG-DASH standard.

This repository contains the source for the Android client, consisting of two components:

* A recorder and uploader that will record 720x480 MPEG-4 videos from the device camera, segment it into 3 second segments and then upload the segments on-the-fly to a designated web server.

* An MPEG-DASH player that will playback the uploaded videos, with a few rate switching techniques to improve the adaptability of video playback on varying network conditions. We utilize predetermined playback buffer thresholds to trigger the switching of video quality.